FROM alpine:3.7
RUN addgroup -S usergroup && adduser -u 2165 -S user -G usergroup
USER 2165
ENTRYPOINT ["cat","/tmp/shadow"]
